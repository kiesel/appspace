<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
 version="1.0"
 xmlns:exsl="http://exslt.org/common"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:func="http://exslt.org/functions"
 xmlns:str="http://exslt.org/strings"
 xmlns:php="http://php.net/xsl"
 extension-element-prefixes="func str"
 exclude-result-prefixes="exsl func php str"
>

  <xsl:output method="xml" encoding="utf-8" doctype-public="HTML"/>

  <xsl:template match="/">
    <html lang="en">
      <head>
        <title>SICK appspace</title>
      </head>
      <body>
        <h1>Welcome @ appspace</h1>

        <xsl:apply-templates select="/formresult/apps"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="apps">
    <xsl:apply-templates match="./app"/>
  </xsl:template>

  <xsl:template match="app">
    App...<br/>
  </xsl:template>

</xsl:stylesheet>