<?php namespace appspace\client;

use webservices\rest\RestClient;
use webservices\rest\RestRequest;

class Appspace {
  private $client;

  public function __construct($endpoint= 'http://localhost:8081') {
    $this->client= new RestClient($endpoint);
  }

  public function retrieveApps() {
    return $this->client->execute(new RestRequest('/apps'))->data();
  }
}