<?php namespace appspace\frontend\state;

use scriptlet\xml\workflow\AbstractState;
use xml\Node;
use webservices\rest\RestClient;
use webservices\rest\RestRequest;
use appspace\client\Appspace;

class StaticState extends AbstractState {

  public function process($request, $response, $context) {
    $node= $response->addFormResult(new Node('apps'));
    foreach ((new Appspace())->retrieveApps() as $app) {
      var_dump($app);
      $node->addChild(new Node('app', null, [
        'name' => $app->name,
        'description' => $app->description,
        'versions'   => $app->versions,
      ]));
    }
  }
}