<?php namespace appspace\frontend\scriptlet;

use scriptlet\xml\workflow\AbstractXMLScriptlet;

class FrontendScriptlet extends AbstractXMLScriptlet {

  /**
   * Sets the responses XSL stylesheet
   *
   * @param   scriptlet.scriptlet.XMLScriptletRequest request
   * @param   scriptlet.scriptlet.XMLScriptletResponse response
   */
  protected function _setStylesheet($request, $response) {
    $response->setStylesheet(sprintf(
      '%4$s.xsl',
      DIRECTORY_SEPARATOR,
      $request->getProduct(),
      $request->getLanguage(),
      $request->getStateName()
    ));
  }


}