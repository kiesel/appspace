<?php namespace appspace;

use util\Date;
use webservices\rest\srv\Response;

#[@webservice]
class AppsHandler {

  #[@webmethod(verb= 'GET', path= '/apps')]
  public function apps() {
    return [ 'apps' => [
        [ 
          'slug' => 'syshealth',
          'name' => 'System health check',
          'description' => 'Monitors sensor health',
          'author' => 'SICK AG',
          'author-id' => 1,
          'price' => null,
          'last_version' => '1.10.3',
          'versions' => ['1.0.0', '1.1.0', '1.10.1', '1.10.3'],
          'last_update' => new Date('2016-11-11 10:00'),
        ]
      ]
    ];
  }

  #[@webmethod(verb= 'GET', path='/app/{slug}/deployments')]
  public function appDeployments($slug) {
    $deployments= [
      'syshealth' => [
        [
          'device' => '1-4-4-9',
          'type' => 'W12-3',
          'id'   => 12345,
          'version' => '1.10.3',
          'actions' => ['remove'],
          'notes'   => [],
        ]
      ]
    ];

    if (!isset($deployments[$slug])) {
      return Response::notFound();
    }

    return ['deployments' => $deployments[$slug]];
  }
}